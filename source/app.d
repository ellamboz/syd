import std.stdio;
import syclasses.diceware;
import syclasses.sy1items;
import std.getopt;
import core.stdc.stdlib;

const string USAGE_MSG = "

syd -- create random patches for sy1

usage: syd [-e] [--factor 0.0..1.0] [--items -1..50] [-s patchfile.sy1] [-o filename.zip]

-e              : enable evolutive patch generation
--factor d      : randomization factor, from 0.0 to 0.1 . Only on evolutive generation
--items  n      : parameters to randomize, from -1 to 50. -1 = all parameters
-s patchfile    : Synth1 patchfile to load as reference
-o filename.zip : zip file to generate. If not specified a random name will be userd
-h              : this screen 

";



void main( string[] args)
{
	bool evolve = false;
	double factor = 0.25;
	int items = 10;
	string sy1patchfile  = "";
	string outputPatchfile = getWord() ~ "_" ~ getWord() ~ ".zip";

	try {
		auto helpInfo = getopt( args,
		"e"         , &evolve,
		"factor"    , &factor,
		"items"     , &items,
		"patchfile" , &sy1patchfile,
		"o"         , &outputPatchfile);

		if (items < -1 || items == 0 || items > 50) {
			writeln ("\nERROR: --items must be -1 or between 1 and 50\n");
			writeln (USAGE_MSG);
			exit(1);
		}

		if (factor < 0.0 || factor > 1.0) {
			writeln ("\nERROR: --factor  between 0.0 and 1.0\n");
			writeln (USAGE_MSG);
			exit(1);
		}

		if (helpInfo.helpWanted) {
			writeln (USAGE_MSG);
		}
		else {
			writeln ("Writing patch data to " ~ outputPatchfile);
			if (evolve) {
				createSy1EvolvedPatchset(outputPatchfile,sy1patchfile,factor, items);
			} else {
				createSy1RandomPatchset(outputPatchfile);
			}
		}
	} catch (Exception ex) {
		writeln ("\nError parsing parameters\n");
		writeln (USAGE_MSG);
		exit(1);

	}
}
