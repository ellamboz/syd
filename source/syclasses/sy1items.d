module syclasses.sy1items;

import syclasses.diceware;
import std.stdio :  writeln, writefln, File;
import std.conv;
import std.string;
import std.random;
import std.file;
import std.path;
import std.zip;

int[] KEYS = [
    0  ,45 ,76 ,1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 ,11 ,12 ,
    13 ,71 ,72 ,14 ,15 ,16 ,17 ,18 ,19 ,20 ,21 ,22 ,23 ,24 ,25 ,
    26 ,27 ,28 ,29 ,30 ,59 ,31 ,32 ,33 ,34 ,65 ,82 ,35 ,83 ,36 ,
    37 ,66 ,64 ,52 ,53 ,54 ,55 ,56 ,60 ,61 ,62 ,63 ,90 ,77 ,78 ,
    79 ,80 ,81 ,38 ,39 ,74 ,40 ,73 ,75 ,84 ,85 ,50 ,51 ,57 ,41 ,
    42 ,43 ,44 ,67 ,68 ,58 ,46 ,47 ,48 ,49 ,69 ,87,
    70 ,91,94,93,89,95,96,97,98,92,86,88
];

MinstdRand0 randomizer;

static this() {
    randomizer = MinstdRand0(unpredictableSeed);
}

public class SyParameter {
    public int idx;
    public int min;
    public int max;
    public int curValue;
    public string desc;

    static SyParameter[int] parameters;

    static this() {
        parameters = [
			0  : new SyParameter(0, 0, 3, "OSCILLATOR 1 WAVE"),
            45 : new SyParameter(45, 0, 127, "FM"),
            76 : new SyParameter(76, 0, 127, "DETUNE"),
            1 : new SyParameter(1, 1, 4, "OSCILLATOR 2 WAVE"),
            2 : new SyParameter(2, 0, 127, "PITCH"),
            3 : new SyParameter(3, 0, 127, "FINE"),
            4 : new SyParameter(4, 0, 1, "TRACK"),
            5 : new SyParameter(5, 0, 127, "MIX"),
            6 : new SyParameter(6, 0, 1, "SYNC"),
            7 : new SyParameter(7, 0, 1, "RING"),
            8 : new SyParameter(8, 0, 127, "PW"),
            9 : new SyParameter(9, -24, 24, "TRANSPOSE"),
            10 : new SyParameter(10, 0, 1, "M. ENV SWITCH"),
            11 : new SyParameter(11, 0, 127, "AMOUNT"),
            12 : new SyParameter(12, 0, 127, "ATTACK"),
            13 : new SyParameter(13, 0, 127, "DECAY"),
            71 : new SyParameter(71, 0, 2, "DEST"),
            72 : new SyParameter(72, 0, 127, "TUNE"),
            14 : new SyParameter(14, 0, 3, "FILTER TYPE"),
            15 : new SyParameter(15, 0, 127, "ATTACK"),
            16 : new SyParameter(16, 0, 127, "DECAY"),
            17 : new SyParameter(17, 0, 127, "SUSTAIN"),
            18 : new SyParameter(18, 0, 127, "RELEASE"),
            19 : new SyParameter(19, 0, 127, "FREQUENCY"),
            20 : new SyParameter(20, 0, 127, "RESONANCE"),
            21 : new SyParameter(21, 0, 127, "AMOUNT"),
            22 : new SyParameter(22, 0, 127, "TRACK"),
            23 : new SyParameter(23, 0, 127, "SATURATION"),
            24 : new SyParameter(24, 0, 1, "VELOCITY AMOUNT"),
            25 : new SyParameter(25, 0, 127, "ATTACK"),
            26 : new SyParameter(26, 0, 127, "DECAY"),
            27 : new SyParameter(27, 0, 127, "SUSTAIN"),
            28 : new SyParameter(28, 0, 127, "RELEASE"),
            29 : new SyParameter(29, 0, 127, "GAIN"),
            30 : new SyParameter(30, 0, 127, "VELOCITY AMOUNT"),
            59 : new SyParameter(59, 0, 1, "ARP SWITCH"),
            31 : new SyParameter(31, 1, 4, "TYPE"),
            32 : new SyParameter(32, 0, 3, "RANGE"),
            33 : new SyParameter(33, 0, 18, "BEAT"),
            34 : new SyParameter(34, 5, 127, "GATE"),
            65 : new SyParameter(65, 0, 1, "DELAY SWITCH"),
            82 : new SyParameter(82, 0, 2, "TYPE"),
            35 : new SyParameter(35, 0, 19, "TIME"),
            83 : new SyParameter(83, 0, 127, "SPREAD"),
            36 : new SyParameter(36, 1, 120, "FEEDBACK"),
            37 : new SyParameter(37, 0, 127, "DRY/WET"),
            66 : new SyParameter(66, 0, 1, "CHORUS SWITCH"),
            64 : new SyParameter(64, 1, 4, "TYPE"),
            52 : new SyParameter(52, 0, 127, "TIME"),
            53 : new SyParameter(53, 0, 127, "DEPTH"),
            54 : new SyParameter(54, 0, 127, "RATE"),
            55 : new SyParameter(55, 0, 127, "FEEDBACK"),
            56 : new SyParameter(56, 0, 127, "LEVEL"),
            60 : new SyParameter(60, 0, 127, "EQ TONE"),
            61 : new SyParameter(61, 0, 127, "FREQUENCY"),
            62 : new SyParameter(62, 0, 127, "LEVEL"),
            63 : new SyParameter(63, 0, 127, "Q"),
            90 : new SyParameter(90, 32, 96, "L-R"),
            77 : new SyParameter(77, 0, 1, "EFFECT SWITCH"),
            78 : new SyParameter(78, 0, 9, "TYPE"),
            79 : new SyParameter(79, 0, 127, "CTRL1"),
            80 : new SyParameter(80, 0, 127, "CTRL2"),
            81 : new SyParameter(81, 0, 127, "LEVEL"),
            38 : new SyParameter(38, 0, 2, "PLAY MODE"),
            39 : new SyParameter(39, 0, 127, "PORTAMENTO"),
            74 : new SyParameter(74, 0, 1, "AUTO"),
            40 : new SyParameter(40, 0, 24, "PB RANGE"),
            73 : new SyParameter(73, 0, 1, "UNISON"),
            75 : new SyParameter(75, 0, 127, "DETUNE"),
            84 : new SyParameter(84, 0, 127, "SPREAD"),
            85 : new SyParameter(85, 0, 48, "PITCH"),
            50 : new SyParameter(50, 0, 127, "LFO1 WHEEL SENS"),
            51 : new SyParameter(51, 0, 127, "SPEED"),
            57 : new SyParameter(57, 0, 1, "LFO1 SWITCH"),
            41 : new SyParameter(41, 1, 7, "DEST"),
            42 : new SyParameter(42, 0, 4, "WAVEFORM"),
            43 : new SyParameter(43, 0, 127, "SPEED"),
            44 : new SyParameter(44, 0, 127, "AMOUNT"),
            67 : new SyParameter(67, 0, 1, "TEMPO SYNC"),
            68 : new SyParameter(68, 0, 1, "KEY SYNC"),
            58 : new SyParameter(58, 0, 1, "LFO2 SWITCH"),
            46 : new SyParameter(46, 1, 7, "DEST"),
            47 : new SyParameter(47, 0, 4, "WAVEFORM"),
            48 : new SyParameter(48, 0, 127, "SPEED"),
            49 : new SyParameter(49, 0, 127, "AMOUNT"),
            69 : new SyParameter(69, 0, 1, "TEMPO SYNC"),
            70 : new SyParameter(70, 0, 1, "KEY SYNC"),
            91:  new SyParameter(91, 0, 0, "AA") ,
            94:  new SyParameter(94, 1, 16, "FF") ,
            93:  new SyParameter(93, 1, 2, "XX") ,
            89:  new SyParameter(89, 1, 64, "64") ,
            95:  new SyParameter(95, 0, 0, "BB") ,
            96:  new SyParameter(96, 1, 1, "CC") ,
            97:  new SyParameter(97, 1, 1, "DD") ,
            98:  new SyParameter(98, 1, 64, "EE") ,
            92:  new SyParameter(92, 0, 1, "WW") ,
            86:  new SyParameter(86, 45057, 45057, "E1E") ,
            87:  new SyParameter(87, 1, 64, "A2A") ,
            88:  new SyParameter(88, 45057, 45057, "W1W")];
    }

    this (int _idx, int _min, int _max, string _desc) {
        idx = _idx;
        min = _min;
        max = _max;
        desc = _desc;
        this.setRandomValue();
    }

    public void setRandomValue()
    {
        curValue = randrange(min, max+1);
    }

    public SyParameter fromParameter(bool randomValue = true)
    {
        auto p = new SyParameter
        (
            this.idx,
            this.min,
            this.max,
            this.desc
        );
        if (randomValue) 
            p.setRandomValue(); 
        else 
            p.curValue = this.curValue;
        return p;
    }

    public void setEvolvedValue(double perc = .25)
    {
        immutable int d1 = curValue - min;
        immutable int d2 = max - curValue;
        int p1 = d1 < 3 ? d1 : cast(int)(cast(float)d1 * perc);
        int p2 = d2 < 3 ? d2 : cast(int)(cast(float)d2 * perc);
        immutable int v = curValue;
        auto v1 = v - p1;
        auto v2 = v + p2;
        if (v1 < v2) {
            curValue = randrange(v1, v2);
        } 
    }

}

public class Sy1Patch
{
    SyParameter[] _parameters;
    SyParameter[int] _paramMap;
    string[] colors = [ "cyan", "red", "green", "blue", "yellow", "magenta" ];

    public string patchName;
    public const string PATCHVERSION = "ver=112";

    public string color()
    {
            return colors[randrange(0,to!int(colors.length))];
    }

    public this()
    {
        patchName = getWords(2);
        foreach (i; KEYS)
        {
            auto p = SyParameter.parameters[i].fromParameter();
            _parameters ~= p;
            _paramMap[i] = p;
        }
    }

    public static Sy1Patch fromSy1File(string sy1Path)
    {
        auto p = new Sy1Patch();
        auto txt = File(sy1Path);
        string[] items;
        
        foreach (line;txt.byLine) 
            items ~= line.idup();

        items = items[3..$];

        foreach (i; items) {
            auto ik = i.split(',');
            if (ik.length == 2) {
                immutable int k = to!int(ik[0]);
                immutable int v = to!int(ik[1]);
                p._paramMap[k].curValue=v;
            }
        }
        txt.close();

        return p;
    }

    public void makeBasePatch()
    {
        _paramMap[0].curValue = 1;
        _paramMap[45].curValue = 0;
        _paramMap[76].curValue = 5;
        _paramMap[1].curValue = 3;
        _paramMap[2].curValue = 39;
        _paramMap[3].curValue = 8;
        _paramMap[4].curValue = 1;
        _paramMap[5].curValue = 40;
        _paramMap[6].curValue = 1;
        _paramMap[7].curValue = 1;
        _paramMap[8].curValue = 89;
        _paramMap[9].curValue = 0;
        _paramMap[10].curValue = 0;
        _paramMap[11].curValue = 123;
        _paramMap[12].curValue = 50;
        _paramMap[13].curValue = 77;
        _paramMap[71].curValue = 2;
        _paramMap[72].curValue = 78;
        _paramMap[91].curValue = 0;
        _paramMap[95].curValue = 0;
        _paramMap[96].curValue = 1;
        _paramMap[97].curValue = 1;
        _paramMap[14].curValue = 2;
        _paramMap[15].curValue = 49;
        _paramMap[16].curValue = 35;
        _paramMap[17].curValue = 59;
        _paramMap[18].curValue = 82;
        _paramMap[19].curValue = 72;
        _paramMap[20].curValue = 70;
        _paramMap[21].curValue = 68;
        _paramMap[22].curValue = 95;
        _paramMap[23].curValue = 119;
        _paramMap[24].curValue = 0;
        _paramMap[25].curValue = 23;
        _paramMap[26].curValue = 105;
        _paramMap[27].curValue = 100;
        _paramMap[28].curValue = 71;
        _paramMap[29].curValue = 75;
        _paramMap[30].curValue = 11;
        _paramMap[59].curValue = 0;
        _paramMap[31].curValue = 4;
        _paramMap[32].curValue = 1;
        _paramMap[33].curValue = 12;
        _paramMap[34].curValue = 54;
        _paramMap[65].curValue = 0;
        _paramMap[82].curValue = 0;
        _paramMap[35].curValue = 9;
        _paramMap[83].curValue = 105;
        _paramMap[36].curValue = 105;
        _paramMap[98].curValue = 64;
        _paramMap[37].curValue = 38;
        _paramMap[66].curValue = 0;
        _paramMap[64].curValue = 1;
        _paramMap[52].curValue = 100;
        _paramMap[53].curValue = 110;
        _paramMap[54].curValue = 10;
        _paramMap[55].curValue = 122;
        _paramMap[56].curValue = 90;
        _paramMap[60].curValue = 83;
        _paramMap[61].curValue = 72;
        _paramMap[62].curValue = 46;
        _paramMap[63].curValue = 46;
        _paramMap[90].curValue = 77;
        _paramMap[77].curValue = 0;
        _paramMap[78].curValue = 1;
        _paramMap[79].curValue = 40;
        _paramMap[80].curValue = 70;
        _paramMap[81].curValue = 45;
        _paramMap[38].curValue = 0;
        _paramMap[94].curValue = 16;
        _paramMap[39].curValue = 40;
        _paramMap[74].curValue = 0;
        _paramMap[73].curValue = 0;
        _paramMap[93].curValue = 4;
        _paramMap[75].curValue = 127;
        _paramMap[84].curValue = 4;
        _paramMap[85].curValue = 2;
        _paramMap[92].curValue = 0;
        _paramMap[40].curValue = 2;
        _paramMap[86].curValue = 45057;
        _paramMap[50].curValue = 64;
        _paramMap[87].curValue = 44;
        _paramMap[88].curValue = 45057;
        _paramMap[51].curValue = 91;
        _paramMap[89].curValue = 43;
        _paramMap[57].curValue = 0;
        _paramMap[41].curValue = 3;
        _paramMap[42].curValue = 0;
        _paramMap[43].curValue = 50;
        _paramMap[44].curValue = 125;
        _paramMap[67].curValue = 0;
        _paramMap[68].curValue = 0;
        _paramMap[58].curValue = 0;
        _paramMap[46].curValue = 6;
        _paramMap[47].curValue = 1;
        _paramMap[48].curValue = 67;
        _paramMap[49].curValue = 112;
        _paramMap[69].curValue = 0;
        _paramMap[70].curValue = 0;
    }

    public void newEvolvedPatch(double evolve = 0.15, int take = -1)
    {
        patchName = getWords(2);
        if (take == -1)
        {
            foreach (k; KEYS)
            {
                _paramMap[k].setEvolvedValue(evolve);
            }
        }
        else
        {            
            auto aShuffledKeys = randomShuffle(KEYS);
            auto sliceKeys = aShuffledKeys[0..take];
            foreach (k;sliceKeys)
            {
                _paramMap[k].setEvolvedValue(evolve);
            }
        }
    }


    public void newRandomPatch()
    {
        patchName = getWords(2);
        foreach (p; _parameters) {
            p.setRandomValue();
        }
    }


    public override string toString()
    {
        string s = "";
        s = s ~ patchName ~ "\n";
        s = s ~ "color=" ~ color ~ "\n";
        s = s ~ PATCHVERSION ~ "\n";
        foreach (p; _parameters) {
            s = s~  to!string(p.idx) ~ "," ~ to!string(p.curValue) ~ "\n";
        }
        return s;
    }
}

void createSy1RandomPatchset (string filename) {
    ZipArchive zip = new ZipArchive();
    for (int h = 1; h <= 128; h++) {
        ArchiveMember syPatch = new ArchiveMember();
        auto patchName = format("%.3s",h) ~ ".sy1";
        auto data = new Sy1Patch().toString();
        syPatch.name = patchName;
        syPatch.expandedData(data.dup.representation);
        syPatch.compressionMethod = CompressionMethod.deflate;
        zip.addMember(syPatch);
    }
    void[] compressedData = zip.build();
    write(filename, compressedData);
}

void createSy1EvolvedPatchset(string filename, string patchFilename, double randomFactor = 0.25, int nItems = 10) {

    Sy1Patch basePatch = new Sy1Patch();
    try {
        basePatch = Sy1Patch.fromSy1File(patchFilename);
    }
    catch (Exception e) {
        writeln ("Problems with patch file " ~ patchFilename);
        basePatch.makeBasePatch();
    }

    ZipArchive zip = new ZipArchive();
    for (int h = 1; h <= 128; h++) {
        ArchiveMember syPatch = new ArchiveMember();
        immutable auto patchName = format("%.3s",h) ~ ".sy1";
        auto data = basePatch.toString();
        syPatch.name = patchName;
        syPatch.expandedData(data.dup.representation);
        syPatch.compressionMethod = CompressionMethod.deflate;
        zip.addMember(syPatch);
        basePatch.newEvolvedPatch(randomFactor, nItems);
    }
    void[] compressedData = zip.build();
    write(filename, compressedData);
}