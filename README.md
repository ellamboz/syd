# Syd

`syd` is a command-line tool for windows written in [D](https://dlang.org) that randomly generates  _external patchsets_, that is zip files containing up to 128  patches for the awesome Synth1 freeware VST plugin, available here: https://www.kvraudio.com/product/synth1-by-daichi-laboratory-ichiro-toda and here: https://daichilab.sakura.ne.jp/softsynth/index.html

External Patchsets can be loaded this way:

When on Synth1 interface, click on **opt**:

![image-20200502203722589](./README.assets/image-20200502203722589.png)

![image-20200502203743311](./README.assets/image-20200502203743311.png)

Select the folder where zip patchsets are stored by clicking on _Browse_ button near ExtBank (zip) directory box.

Once the directory is set, you can select an external patch by clicking on the patch name or the patchset name (the cyan labels on black background and you'll be prompted this dialog)

![image-20200502204008864](./README.assets/image-20200502204008864.png)

Expand the **External (zip)** tree 

![image-20200502204130072](./README.assets/image-20200502204130072.png)

And you'll see all external banks available 

![image-20200502204202783](./README.assets/image-20200502204202783.png)

​	Being a command line tool, it's got some options: here they are.

~~~

syd -- create random patches for Synth1

usage: syd [-e] [--factor 0.0..1.0] [--items -1..50] [-s patchfile.sy1] [-o filename.zip]

-e              : enable evolutive patch generation
--factor d      : randomization factor, from 0.0 to 0.1 . Only on evolutive generation
--items  n      : parameters to randomize, from -1 to 50. -1 = all parameters
-s patchfile    : Synth1 patchfile to load as reference
-o filename.zip : zip file to generate. If not specified a random name will be userd
-h              : this screen

~~~

**Evolutive** patch generation means that you provide `syd` a `.sy1` patch file as a basis, and you generate a patchset full of mutations of the base patch. 

Executable images for win32 and win64 can be found in `./rel32 ` and `./rel64` subdirs

### Are these random patches any good?

After toying with this tool, I found that 2/3 of these patches range from mediocre to bad, even for obscure drone noise music. 

Of the remaining third, most of it can be used only for drone or need some tweaking. Thus, only a mere fraction can be used "as-is" in normal songs. Yet, **there are chances** that you can get treated with some otherworldly sounds that make you go wow. Moreover, this tool is lightweight and free, and will certainly save you time when exploring Synth1 settings. 